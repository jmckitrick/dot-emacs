;;; -*- mode: emacs-lisp; -*-

(eval-when-compile (require 'subr-x))

;; Set up lib path for native compilation.
(setenv "LIBRARY_PATH"
        (string-join
         '("/opt/homebrew/Cellar/gcc/14.2.0/lib/gcc/14/"
           "/opt/homebrew/Cellar/gcc/14.2.0/lib/gcc/14/gcc/aarch64-apple-darwin23/14")
         ":"))
