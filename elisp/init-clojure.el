;;; -*- mode: emacs-lisp; -*-

;; NB: Options can also be set in Directory Local Variables.

(use-package cider
  :ensure t
  :config
  (setq
   nrepl-buffer-name-show-port t
   cider-repl-print-length 50
   cider-repl-result-prefix ""
   cider-repl-use-clojure-font-lock t
   cider-prompt-for-symbol nil

   nrepl-log-messages nil
   cider-repl-buffer-size-limit 100000000
   nrepl-hide-special-buffers t
   cider-repl-pop-to-buffer-on-connect t
   cider-popup-stacktraces t
   cider-repl-use-pretty-printing t
   cider-auto-select-error-buffer t
   cider-repl-display-in-current-window nil
   cider-repl-history-file "~/.emacs.d/nrepl-history"
   cider-prompt-save-file-on-load nil
   cider-repl-display-help-banner nil)
  ;; Not sure why this is here, but if we see cider completion issues,
  ;; revisit this topic (orderless)
  ;:hook
  ;(cider-mode . (lambda () (setq-local completion-styles '(basic))))
  ;(cider-repl-mode . (lambda () (setq-local completion-styles '(basic))))
  )

(setq cider-repl-display-output-before-window-boundaries t)

(defun my-cider-mode-hook ()
  (eldoc-mode 1)
  (cider-eldoc-setup)
  ;;(company-mode)
  (use-package init-lisp)
  (my-lisp-setup))

(defun my-cider-repl-mode-hook ()
  (eldoc-mode 1)
  (cider-eldoc-setup)
  ;;(company-mode)
  (use-package init-lisp)
  (my-lisp-setup))

(defun my-clojure-mode-hook ()
  (require 'clj-refactor)
  (clj-refactor-mode 1)
  (yas-minor-mode 1)
  (cljr-add-keybindings-with-prefix "<f9>")
  (cljr-add-keybindings-with-prefix "C-c C-g"))

(add-hook 'cider-mode-hook #'my-cider-mode-hook)
(add-hook 'cider-repl-mode-hook #'my-cider-repl-mode-hook)

(when (member 'refactor jcm/extras)
  (add-hook 'clojure-mode-hook #'my-clojure-mode-hook))

(provide 'init-clojure)
